<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include 'header.php';
include 'Membro.php';
$relatorio = new Membro();


include 'validarSeUsuarioEstarLogado.php';


?>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <?php
    include 'headerTop.php'
    ?>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?php
    include 'menuDash.php'
    ?>

    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 id="titulo" class="page-title">Relatorio de dizimos</h4>
                    <div class="ms-auto text-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="dashbord.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page"> <a href="cadastroMembro.php">Novo Membro</a> </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
</div>
</div>
<div class="bg-white container-xxl mt-1">
    <div class=" d-flex relarotios" >
        <div class="col-10 text" style="font-size: 13px;">
            <?php if ( isset($_GET['sucesso']) ) : ?>
                <div class="alert alert alert-success alert-dismissible  show cor2" role="alert">
                    <strong>Excluido</strong> Membro excluido
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php elseif (isset($_GET['update']) ) : ?>
                <div class="alert alert alert-success alert-dismissible  show cor2" role="alert">
                    <strong>Sucesso</strong> Update feito com sucesso.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php elseif (isset($_GET['naoCadastrado']) ) : ?>
                <div class="alert alert-warning alert-dismissible  show cor2" role="alert">
                    <strong>Erro</strong> Tente novamente.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php elseif (isset($_GET['urlvazia=ok']) ) : ?>
                <div class="alert alert-warning alert-dismissible  show cor2" role="alert">
                    <strong>Erro</strong> Você esta tentando acessa pela Url direto.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>

            <?php endif  ?>

            <table border="0" cellspacing="3" cellpadding="5">

                </tbody></table>
            <table id="example"  class="stripe row-border order-column nowrap " style="width:100%">
                <thead>
            
                <hr>
                <tr>
                    <th>id</th>
                    <td>Nome</td>
                    <td>Igreja</td>
                    <td>Email</td>
                    <td>Cidade</td>
                    <td>Bairro</td>
                    <td>Endereço</td>
                    <td>Telefone</td>
                    <td>Batismo</td>
                    <td>Batismo água </td>
                    <td>Data nascimento</td>
                    <td>Cargo</td>
                    <td>Situação</td>
                    <th>Botoẽs</th>


                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($relatorio->pegarMembro() as $membro) : 
                   $data_batismo = $membro['Batismo_agua'];
                  
                   $data_batismo_espirito_santo = $membro['data_batismo_espirito_santo'];
                   $data_nascimento = $membro['data_nascimento'];
                ?>
                <tr>
                    <td><?php echo $membro['Id'] ?></td>
                    <td><?php echo $membro['Nome_membro'] ?></td>
                    <td><?php echo $membro['igreja'] ?></td>
                    <td><?php echo $membro['Email_dizimista'] ?></td>
                    <td><?php echo $membro['Cidade'] ?></td>
                    <td><?php echo $membro['Barrio'] ?></td>
                    <td><?php echo $membro['Endereco'] ?></td>
                    <td><?php echo $membro['Telefone'] ?></td>
                    <td><?php if($data_batismo == null){echo '';}else{ echo date('d-m-Y', strtotime($data_batismo));}?> </td>
                    <td><?php if($data_batismo_espirito_santo == null){echo '';}else{ echo date('d-m-Y', strtotime($data_batismo_espirito_santo));}?> </td>
                    <td><?php if($data_nascimento == null){echo '';}else{ echo date('d-m-Y', strtotime($data_nascimento));}?> </td>
                    <td><?php echo $membro['Cargo'] ?></td>
                    <td><?php echo $membro['Situacao'] ?></td>
                    <td>
                        <form class="form_bt" method="GET" action="relatoriodespesas.php">
                            <button  type="button"  class="view_data botao_detalhes1"  data-toggle="modal" data-target="#modalVisualizarDizimos<?php echo $membro['Id']; ?>">
                                <i class="mdi mdi-trending-up"></i>
                            </button>
                            <button  type="button"  class="view_data botao_detalhes2" data-toggle="modal" data-target="#modalVisualizar<?php echo $membro['Id']; ?>">
                                <i class="fas fa-paperclip"></i>
                            </button>

                            <button type="button"  class="botao_detalhes" data-toggle="modal" data-target="#editar<?php echo $membro['Id']; ?>">
                                <i class="fas fa-pencil-alt"></i>
                            </button>

                            <button type="button"  data-toggle="modal" data-toggle="modal" data-target="#excluir<?php echo $membro['Id']; ?>" class="botao_detalhes3">
                            <i class="fas fa-trash-alt"></i>
                            </button>
                            <button type="button"  data-toggle="modal" data-toggle="modal" data-target="#pdf<?php echo $membro['Id']; ?>" class="botao_detalhes4">
                                <i class="far fa-file-pdf"></i>
                            </button>

<!--                              <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal--><?php //echo $membro['id']; ?><!--">Visualizar</button>-->
                        </form>
                    </td>
                </tr>



                <!-- Fim Editar -->
                <div class="modal" id="editar<?php echo $membro['Id']; ?>" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">

                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                        <h4 class="modal-title text-center" id="myModalLabel"><?php echo $membro['igreja']; ?></h4>
                                        <h4 class="modal-title text-center" id="myModalLabel"><?php echo $membro['Id']; ?></h4>
                                    </div>
                                    <div class="modal-body">
                                    <form action="validaUpdateMembro.php" method="post">
                                        <div class=" m-3">
                                            </label> <input hidden class="input-group border p-2" name="id" value="<?php echo $membro['Id']; ?>">
                                            <label class="input-group-text p-2 ">  Membro </label> <input class="input-group border p-2" name="nome_membro" value="<?php echo $membro['Nome_membro']; ?>">  <hr>
                                            <label class="input-group-text p-2">  Membro </label> <input class="input-group border p-2" name="email_dizimista" value="<?php echo $membro['Email_dizimista']; ?>">  <hr>
                                            <label class="input-group-text p-2" > Fone : </label> <input class="input-group border p-2" name="telefone"  value="<?php echo $membro['Telefone']; ?>" >  <hr>
                                            <label class="input-group-text p-2" > Cidade : </label> <input class="input-group border p-2" name="cidade"  value=" <?php echo $membro['Cidade']; ?>">    <hr>
                                            <label class="input-group-text p-2" >Bairro : </label> <input class="input-group border p-2" name="barrio"  value="<?php echo $membro['Barrio']; ?> ">   <hr>
                                            <label class="input-group-text p-2" >Enderenço : </label> <input class="input-group border p-2" name="endereco"  value=" <?php echo $membro['Endereco']; ?>">    <hr>
                                            <label class="input-group-text p-2" >Batismo : </label> <input class="input-group border p-2"    name="batismo_agua"  value="<?php echo $membro['Batismo_agua']; ?> ">  <hr>
                                            <label class="input-group-text p-2"  >Cargo : </label> <input class="input-group border p-2" require name="cargo"  value=" <?php echo $membro['Cargo']; ?>  ">  <hr>
                                            <label class="input-group-text p-2" >Situação : </label> <input class="input-group border p-2" name="situacao"  value=" <?php echo $membro['Situacao']; ?> ">   <hr>
                                            <button type="submit" name="salvar" value="salvar" class=" btn-primary">Salvar</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                                <!-- Inicio Modal excluir-->
                                <div class="modal" id="excluir<?php echo $membro['Id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                                <h4 class="modal-title text-center" id="myModalLabel">Tem certeza que deseja excluir ?</h4>
                                                <h4 class="modal-title text-center" id="myModalLabel"><?php echo $membro['Id']; ?></h4>
                                            </div>

                                            <div class="modal-body">
                                                <p class="form-control"> Essa é uma operação sem volta</p>
                                                <form method="POST" action="validaUpdateMembro.php">
                                                    <input type="text" name="id" hidden value="<?php echo $membro['Id'] ?>">
                                                    <div class="modal-footer">
                                                        <button type="button" class=" btn-secondary" data-dismiss="modal">fechar</button>
                                                        <button type="submit" class=" btn-primary" name="excluir"> Excluir</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

        <div class="modal " id="pdf<?php echo $membro['Id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title text-center" id="myModalLabel"><?php echo $membro['igreja']; ?></h4>
                        <h4 class="modal-title text-center" id="myModalLabel"><?php echo $membro['Id']; ?></h4>

                    </div>
                    <div class="modal-body">
                        <form action="../pdf/index.php" method="post">
                            <div class=" m-3">



                                </label> <input hidden class="input-group border p-2" name="id" value="<?php echo $membro['Id']; ?>">
                                <label class="input-group-text p-2 ">  Tipo da Carta </label>
                                <select class="form-select" aria-label="Default select example" name="tipoCarta">
                                    <option >Carta de Recomendação</option>
                                    <option >Carta de Mudança</option>
                                </select>
                                <hr>
                                <label class="input-group-text p-2">  Igreja </label> <input class="input-group border p-2" name="igreja">  <hr>
                                <label class="input-group-text p-2 ">  Membro </label> <input class="input-group border p-2" name="nome_membro" value="<?php echo $membro['Nome_membro']; ?>">  <hr>
                                <label class="input-group-text p-2" >Batismo : </label> <input class="input-group border p-2"    name="batismo_agua"  value="<?php echo $membro['Batismo_agua']; ?> ">  <hr>
                                <label class="input-group-text p-2"  >Cargo : </label> <input class="input-group border p-2" name="cargo"  value=" <?php echo $membro['Cargo']; ?>  ">  <hr>
                                <label class="input-group-text p-2"  >Email : </label> <input class="input-group border p-2" name="email"  value=" <?php echo $membro['Email_dizimista']; ?>  ">  <hr>
                                <button type="submit" name="salvar" value="salvar" class="btn-primary ">Gerar carta</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>






                                    <!-- Inicio Modal detalhes membros -->
                <div class="modal " id="modalVisualizar<?php echo $membro['Id']; ?>" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl " >
                        <div class="modal-content">
                            <div class="modal-header">
                                <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                <h4 class="modal-title text-center active" id="myModalLabel"><?php echo $membro['igreja']; ?></h4>
                                <h4 class="modal-title text-center" id="myModalLabel"><?php echo $membro['Id']; ?></h4>

                            </div>
                            <div class="modal-body">

                                <div style="width: 45% ; float: left;">
                                    <label class="input-group-text active">  Membro </label> <i class="mdi mdi-account" ></i> <?php echo $membro['Nome_membro']; ?><hr>

                                    <label class="input-group-text active" > Fone : </label> <i class="mdi mdi-cellphone"></i> <?php echo $membro['Telefone']; ?>    <hr>
                                    <label class="input-group-text active" > Email : </label> <i class="mdi mdi-email-open-outline"></i> <?php echo $membro['Email_dizimista']; ?>    <hr>

                                    <label class="input-group-text active" > Cidade : </label> <i class="mdi mdi-city"></i> <?php echo $membro['Cidade']; ?>    <hr>

                                    <label class="input-group-text active" >Bairro : </label> <i class="mdi mdi-home-outline"></i> <?php echo $membro['Barrio']; ?>    <hr>


                                    <label class="input-group-text active" >Enderenço : </label> <i class="mdi mdi-map-marker-radius"></i> <?php echo $membro['Endereco']; ?>    <hr>

                                     <label class="input-group-text active" >Batismo : </label> <i class="mdi mdi-opacity"></i> <?php echo $membro['Batismo_agua']; ?>    <hr>
                                    <label class="input-group-text active" >Cargo : </label> <i class="mdi mdi-wrench"></i> <?php echo $membro['Cargo']; ?>    <hr>
                                 <label class="input-group-text active" >Situação : </label> <i class="mdi mdi-stop-circle-outline"></i> <?php echo $membro['Situacao']; ?>    <hr>
                              </div>



                              <div class="modal-body">

                                <div style="width: 45% ; float: right">
                
                                    <ul class="list-group">
                                        <li class="list-group-item active" aria-current="true">Dizimos</li>
                                        <?php  
                                            foreach ( $relatorio->perfilFinanceiroDizimista($membro['Id']) as $value) :
                                            ?>
                                        <li class="list-group-item"><?php echo "Dia " . " <o style='color:green'>". date('d-m-Y', strtotime($value['data'] )) . "</o>"    ?>  no valor  <label>  <?php echo number_format($value['valor'] , 2, ',', '.') ?> </label>  </li><hr>

                                        <?php endforeach ?>
                                    </ul>


                                    <ul class="list-group">
                                        <li class="list-group-item active" aria-current="true">Valor Total</li>
                                        
                                        <li class="list-group-item">  Total  <label>  <?php echo number_format($relatorio->perfilFinanceiroDizimistaValorTotal($membro['Id']) , 2, ',', '.') ?> </label>  </li><hr>

                                        
                                    </ul>
                                    
                                </div>
                            </div>  
                    </div>
                </div>
                <div>




        <?php endforeach ?>
        </tbody>
        </table>

    </div>
</div>
</div>
<!-- Column -->
</div>
<!-- ============================================================== -->
<!-- Sales chart -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-body">
                <div class="d-md-flex align-items-center">
                    <!--aqui pode ter um card-->
                </div>
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-9">
                        <!--AQUI PODE FICAR UM CARD-->
                    </div>
                    <div class="col-lg-3">
                        <div class="row">
                            <!--aqui fica um card a direita-->
                        </div>
                    </div>
                </div>
                <!-- column -->
            </div>
        </div>
    </div>
</div>

</div>

</div>
<?php include 'footer.php' ?>
</body>

</html>
