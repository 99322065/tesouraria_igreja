<?php

include '../Conexao/Conexao.php';

class ExcluirDespesa extends Conexao
{
    public  function excluir($param)
    {
//        var_dump($param) ; die();
        $excluirDespesa = "DELETE FROM despesas WHERE id=:id";
        $excluirDespesa = $this->db->prepare($excluirDespesa);
        $excluirDespesa->bindValue(':id',$param['id']);
        $excluirDespesa->execute();
        if ($excluirDespesa->rowCount() > 0){
            header("location: relatoriodespesas.php?sucesso=ok");
        }else{
            header("location: relatoriodespesas.php?naoCadastrado=ok");
        }
    }
}