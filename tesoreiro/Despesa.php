<?php
include 'Upload.php';
include 'LancarOferta.php';


class Despesas extends LancarOferta{

    public function cadastrodeDespesasEmgeralDasIgrejas($param){
        $fk_igreja          = $param['fk_igreja'];
        $mes                = $param['mes'];
        $data               = $param['data'];
        $valor              = $param['valor'];
        $tipoDespesa        = $param['tipoDespesa'];
        $observacoes        = $param['observacoes'];

        $new_name= Upload::uploadImagem();

        $valor = $this->converterNumeroParaNegativo($valor);

        $despesasDaIgrejaEmgeral = "INSERT INTO despesas (fk_igreja,mes,data,valor,tipoDespesa,observacoes,arquivo) values 
        (:fk_igreja,:mes,:data,:valor,:tipoDespesa,:observacoes,:arquivo)";
        $despesasDaIgrejaEmgeral = $this->db->prepare($despesasDaIgrejaEmgeral);
        $despesasDaIgrejaEmgeral->bindValue(':fk_igreja',$fk_igreja);
        $despesasDaIgrejaEmgeral->bindValue(':mes',$mes);
        $despesasDaIgrejaEmgeral->bindValue(':data',$data);
        $despesasDaIgrejaEmgeral->bindValue(':valor',$valor);
        $despesasDaIgrejaEmgeral->bindValue(':tipoDespesa',$tipoDespesa);
        $despesasDaIgrejaEmgeral->bindValue(':observacoes',$observacoes);
        $despesasDaIgrejaEmgeral->bindValue(':arquivo',$new_name);

        $despesasDaIgrejaEmgeral->execute();
        if( $despesasDaIgrejaEmgeral->rowCount() > 0 ){
            header("location: despesas.php?sucesso=ok");
        }else{
            header("location: despesas.php?naoCadastrado=ok");
        }
    }

    public function converterNumeroParaNegativo($numero){
        return  $numero * -1;
    }

}