<?php

include 'validarSeUsuarioEstarLogado.php';

?>
<header>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
</header>
<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="pt-4">
                <li class="active">
                    <span aria-expanded="false">
                        <a href="dashbord.php"><i class="h4 mdi mdi-view-dashboard"></i> <label class="ms-1"> Dashboard </label></a>
                    </span>
                </li>

                <li>
                    <a href="#" class="feat-btn">
                        <span class="fas fa-caret-down first"></span>
                        <i class="h4 mdi mdi-account-multiple-plus"></i> <label class="ms-1"> Cadastros  </label>
                    </a>
                    <ul class="feat-show">
                        <li><a href="cadastros.php">
                                <i class="h5 mdi mdi-home"></i> <label class="ms-1"> Congregação  </label></a></li>
                        <li><a href="cadastroMembro.php">
                                <i class="h5 mdi mdi-human-greeting"></i> <label class="ms-1"> Membros  </label></a></li>
                            </a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="serv-btn">
                        <span class="fas fa-caret-down second"></span>
                        <i class="h4 mdi mdi-cash"></i> <label class="ms-1"> Financeiro </label></a>
                    </a>
                    <ul class="serv-show">
                        <li><a href="financeiro.php">
                                <i class="h5 mdi mdi-cash-multiple"></i> <label class="ms-1"> Lançar Dizimo  </label></a></li>
                            </a></li>
                        <li><a href="lancarofertas.php">
                                <i class="h5 mdi mdi-cards"></i> <label class="ms-1"> Lançar Ofertas  </label></a></li>
                            </a></li>
                        <li><a href="despesas.php">
                                <i class="h5 mdi mdi-square-inc-cash"></i> <label class="ms-1"> Lançar Despesas  </label></a></li>
                            </a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="btn-relatorio">
                        <span class="fas fa-caret-down second"></span>
                        <i class="h4 mdi mdi-chart-areaspline"></i> <label class="ms-1"> Relatorios </label>
                    </a>
                    <ul class="relatorio-show">
                        <li>
                            <a href="relatorio.php">
                                <i class="h5 mdi mdi-chart-line"></i> <label class="ms-1">Relatorio dizimos</label>
                            </a></li>
                            <li><a href="relatorio_apresentavel.php">
                                <i class="h5 mdi mdi-chart-line"></i> <label class="ms-1">Relatorio apresentavel</label>
                            </a></li>
                        <li>
                            <a href="ralatorioMembros.php">
                                <i class="h5 mdi mdi-human-greeting"></i> <label class="ms-1">Relatorio membros</label>
                            </a></li>
                        <li><a href="relatoriodespesas.php">
                                <i class="h5 mdi mdi-file-document"></i> <label class="ms-1">Relatorio despesas</label>
                            </a></li>
                    </ul>
                </li>
                <!---->
            </ul>

        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->

    <script>
        $('.btn').click(function(){
            $(this).toggleClass("click");
            $('.sidebar').toggleClass("show");
        });
        $('.feat-btn').click(function(){
            $('nav ul .feat-show').toggleClass("show");
            $('nav ul .first').toggleClass("rotate");
        });
        $('.serv-btn').click(function(){
            $('nav ul .serv-show').toggleClass("show1");
            $('nav ul .second').toggleClass("rotate");
        });

        $('.btn-relatorio').click(function(){
            $('nav ul .relatorio-show').toggleClass("show2");
            $('nav ul .second').toggleClass("rotate");
        });

        $('nav ul li').click(function(){
            $(this).addClass("active").siblings().removeClass("active");
        });
    </script>
</aside>
