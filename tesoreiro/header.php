<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Matrix lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Matrix admin lite design, Matrix admin lite dashboard bootstrap 5 dashboard template" />
    <meta name="description"
        content="Matrix Admin Lite Free Version is powerful and clean admin dashboard template, inpired from Bootstrap Framework" />
    <meta name="robots" content="noindex,nofollow" />
    <title> Igreja AD-53 </title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
    <!-- Favicon icon -->
    <link rel="stylesheet" href="../front-end/Views/cssTest/style.css">
    <link rel="icon" type="image/png" sizes="16x16" href="../front-end/Views/assets/images/AD.jpg" />
    <!-- Custom CSS -->
    <link href="../front-end/Views/assets/libs/flot/css/float-chart.css" rel="stylesheet" />
    
    <!-- Custom CSS -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="https://getbootstrap.com/docs/5.2/assets/css/docs.css" rel="stylesheet">

    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"></script> -->
    <script src=" https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css "></script>
    <script src=" https://cdn.datatables.net/fixedheader/3.2.4/css/fixedHeader.dataTables.min.css"></script>
    <script src=" https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js "></script>
    <script src=" https://cdn.datatables.net/responsive/2.3.0/js/responsive.bootstrap.min.js "></script>

    <!-- Css -->
    <link href="stylos.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
  <link rel="icon" type="image/png" sizes="16x16" href="../front-end/assets/images/AD.jpg" />
  <link href="../front-end/Views/dist/css/style.min.css" rel="stylesheet" />
    <script>
        <script>
            $(document).ready(function(){
            $(document).on('click', '.delete-row', function(e){
                e.preventDefault();
                id = $(this).attr('data-id');
            });
            $(document).on('click', '#deleteItem', function(e){
            window.location = 'delete/' + id;
        });
        });
    </script>
    </script>
</head>