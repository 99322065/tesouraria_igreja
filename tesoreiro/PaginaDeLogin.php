<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="styleLogin.css">
</head>
<?php include 'header.php'?>
<body>
    <div class="main-login" style="background: url('../ufc.png') ;">
        <div class="">
            <!--<h1><br>Faça Login<br>E entre para nosso time</h1>-->
        </div>
        <form method="post" action="validaLogin.php" >
        <div class="right-login" >
            <div class="card-login">
                <h1>LOGIN</h1>
                <div class="textfield">
                    <label for="usuario">Usuário</label>
                    <input type="email" name="email" placeholder="Usuário">
                </div>
                <div class="textfield">
                    <label for="senha">Senha</label>
                    <input type="password" name="senha" placeholder="Senha"> 
                </div>
                <button type="submit" name="login" class="btn-login">Login</button>
                <?php if (isset($_GET['senhaErrada']) ) : ?>
                    <div class="alert " role="alert" style="background: #d95c5c; color: white" >
                        <strong>Erro</strong> Usuario ou senha errado.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    </form>
</body>
<?php include 'footer.php' ?>
</html>