<?php
    include 'Query.php';
    $query = new Query();

include 'validarSeUsuarioEstarLogado.php';

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include 'header.php'?> 
<!-- Latest compiled and minified CSS -->

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

        <?php
        include 'headerTop.php'
        ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php
        include 'menuDash.php'
        ?>


        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Assembleia de Deus AD53 </h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Library
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid tamanhodoInpunt">
                <div class="row">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body ">
                                    <?php 
                                        
                                    if ( isset($_GET['sucesso'])) : ?>
                                      
                                    <div class="alert alert-success alert-dismissible show  cor2" role="alert">
                                    <strong>Sucesso</strong> Novo membro cadastrado.
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                    <?php elseif ( isset($_GET['naoCadastrado'])) : ?>
                                        <div class="alert alert-danger alert-dismissible  show cor2" role="alert">
                                            <strong>Erro</strong> Tente novamente .
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                
                                    <?php elseif ( isset($_GET['duplicado'])) : ?>
                                        <div class="alert alert-danger alert-dismissible  show cor2" role="alert">
                                            <strong>Já existe esse valor</strong> Verifique as informação risco de duplicidade .
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    <?php endif  ?>

                                    <form action="validaFormularioCadastrosDizimistas.php" method="post" autocomplete="off">
                                        <div id="bt" class="form-row">
                                            <label>
                                                <h4>Lançar dizimo</h4>
                                            </label>
                                           



                                            <div class="lista">
                                                <select class="selectpicker" data-width="100%" data-size="5" data-show-subtext="true" data-live-search="true" aria-label="Default select example" name="fk_dizimista" required>
                                                    <?php foreach ($query->getSelect('membros') as $dizimistas):  ?>
                                                        <option class="" value='<?php echo $dizimistas['id'] ?>'><?php echo $dizimistas['id'] . " - " . $dizimistas['nome_membro'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col lista">
                                                <input type="month" name="mes" required class="form-control input" placeholder="mes">
                                            </div>
                                            <div class="col lista">
                                                <input type="date" name="data" required class="form-control input" placeholder="data">
                                            </div>
                                            
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text input">Valor R$: </span>
                                                </div>
                                                <input type="text" name="valor" class="form-control input" step="00.010">
                                            </div>

                                            <button type="submit" name="cadastrar" class="botao" >Lançar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- ============================================================== -->
    <!-- Sales chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-md-flex align-items-center">
                        <!--aqui pode ter um card-->
                    </div>
                    <div class="row">
                        <!-- column -->
                        <div class="col-lg-9">
                            <!--AQUI PODE FICAR UM CARD-->
                        </div>
                        <div class="col-lg-3">
                            <div class="row">
                                <!--aqui fica um card a direita-->
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                </div>
            </div>
        </div>
    </div>
    </div>

 <?php include 'footer.php' ?>
</body>

</html>