<?php

include 'validarSeUsuarioEstarLogado.php';

?>
<ul id="sidebarnav" class="pt-4">
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashbord.php" aria-expanded="false"><i class="mdi mdi mdi-view-dashboard"></i><span class="hide-menu">Principal</span></a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="relatorio.php" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Relatório de dizimos geral</span></a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="relatorio_dizimo_mes.php" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Relatório de dizimos mês</span></a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="ralatorioMembros.php" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Relatório de membros</span></a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="relatoriodizimos.php" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Relatório de dízimos</span></a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="relatoriodedizimistas.php" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Relatório de dizimistas</span></a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="relatoriodeofertasdoscultos.php" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Relatório de ofertas nos cultos</span></a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="relatoriodespesas.php" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Relatório de despesas</span></a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="relatoriogeral.php" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">Relatório geral</span></a>
            </li>
            <!---->
          </ul>