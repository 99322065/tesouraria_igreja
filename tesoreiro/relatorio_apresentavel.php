<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include 'header.php';
include 'RelatorioGeral.php';
$relatorio = new RelatotioGeral();


if (isset($_POST['id'])){
    $relatorio->setId($_POST['id']);
    $relatorio->excluirDizimos('dizimimos');
}

if(isset($_POST['de']) and $_POST['ate']){

  $relatorio->setDe($_POST['de']);
  $relatorio->setAte($_POST['ate']);
}
include 'validarSeUsuarioEstarLogado.php';


?>

<body>
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
    <div class="lds-ripple">
      <div class="lds-pos"></div>
      <div class="lds-pos"></div>
    </div>
  </div>
  <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
      <?php
      include 'headerTop.php'
      ?>
      <!-- ============================================================== -->
      <!-- End Topbar header -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- ============================================================== -->
      <?php
      include 'menuDash.php'
      ?>


      <div class="page-wrapper">
      <div class="page-breadcrumb">
        <div class="row">
          <div class="col-12 d-flex no-block align-items-center">
            <h4 id="titulo" class="page-title">Relatorio dos dizimistas</h4>
            <div class="ms-auto text-end">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="dashbord.php">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page"> <a href="financeiro.php"> Novo dizimo</a> </li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
  </div>
  </div>

  <div class="bg-white container-xxl mt-4 ">
  <div class=" d-flex relarotios" >
   
    <div class="col-10 text" style="font-size:13px ;">
          <?php if ( isset($_GET['sucesso']) ) : ?>
              <div class="alert alert alert-success alert-dismissible  show cor2" role="alert">
                  <strong>Excluido</strong> Dizimo excluido
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
          <?php elseif (isset($_GET['update']) ) : ?>
              <div class="alert alert alert-success alert-dismissible  show cor2" role="alert">
                  <strong>Sucesso</strong> Update feito com sucesso.
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
          <?php elseif (isset($_GET['naoCadastrado']) ) : ?>
              <div class="alert alert-warning alert-dismissible  show cor2" role="alert">
                  <strong>Erro</strong> Tente novamente.
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
          <?php elseif (isset($_GET['urlvazia=ok']) ) : ?>
              <div class="alert alert-warning alert-dismissible  show cor2" role="alert">
                  <strong>Erro</strong> Você esta tentando acessa pela Url direto.
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
          <?php endif  ?>
          <form method="post"  style="margin-left: 600px ; width: 50%; margin-bottom: -25px;" action="relatorio_apresentavel.php">
                                <div class="row">
                                    <div class="col mt-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input" id="inputGroupPrepend">De</span>
                                            </div>
                                            <input type="date" class="form-control input" id="validationCustomUsername" name="de" aria-describedby="inputGroupPrepend">

                                        </div>
                                    </div>
                                    <div class="col mt-4 ">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text input" id="inputGroupPrepend">Ate</span>
                                            </div>
                                            <input type="date" class="form-control input" id="validationCustomUsername" name="ate"  aria-describedby="inputGroupPrepend">

                                            <button type="submit" class="btn-primary " aria-label="Alinhar na esquerda">
                                                <span class="glyphicon glyphicon-search " >Filtrar</span>
                                            </button>

                                        </div>
                                    </div>
                                </div>
                  </form>
                  <?php 
                  
                  $valorTotal = array_sum(array_column($relatorio->relatoriodedizimistas(),'Valor'));
                  $quatidade = array_count_values (array_column($relatorio->relatoriodedizimistas(),'id'));
                  $contarDizimistas =  0;
                  foreach ($quatidade as $quan) {
                    $contarDizimistas = $quan + $contarDizimistas ;
                  }?>
                  
                  <button type="button" class="btn btn-primary" style="font-size:small ;">
                  Valor dizimos <span class="badge badge-light"><?php  echo  "R$ " . number_format($valorTotal, 2, ',', '.'); ?></span>
                 </button>
                 <button type="button" class="btn btn-danger" style="font-size:small ;">
                  Quantidade <span class="badge badge-light"><?php  echo number_format($contarDizimistas , 2, ',', '.'); ?></span>
                 </button>
      <table border="0" cellspacing="10" cellpadding="15">
       
    </tbody></table>
 
    <table id="example"  class="stripe row-border order-column nowrap" style="width:100%">
          <thead>
      
        <hr>
            <tr>
                <th>Id</th>
                <th>Nome</th>
            <!--   <th>Igreja</th> -->
            <!--  <th>Cargo</th> -->
            <!--   <th>Mês</th> -->
               <th>Data</th> 
               <th>Valor</th> 
              <!-- <th>Botoẽs</th> -->
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($relatorio->relatoriodedizimistas() as $relatorioDizimos) : ?>
              <tr>
              <?php
                
                $mes = $relatorioDizimos['Mês'];
                $data = $relatorioDizimos['Data'];

                
               ?>
                  <td><?php echo $relatorioDizimos['id'] ?></td>
                  <td><?php echo $relatorioDizimos['Nome'] ?></td>
               <!-- <td><?php  $relatorioDizimos['Igreja'] ?></td>-->
               <!-- <td><?php  $relatorioDizimos['tipo'] ?></td> -->
               <!--  <td><?php  date('m-Y', strtotime($mes));?></td> -->
                <td><?php echo date('d-m-Y', strtotime($data));?></td> 
                 <td><?php echo number_format($relatorioDizimos['Valor'], 2, ',', '.');   ?></td> 
                <!--  <td>
                      <form class="form_bt" method="GET" action="relatoriodespesas.php">
                          <button  type="button"  class="view_data botao_detalhes1" data-toggle="modal" data-target="#modalVisualizarDizimos<?php echo $relatorioDizimos['Id']; ?>">
                              <i class="mdi mdi-trending-up"></i>
                          </button>
                                                  <button  type="button"  class="view_data botao_detalhes2" data-toggle="modal" data-target="#modalVisualizar--><?php //echo $relatorioDizimos['Id']; ?><!--">-->
                          <!--                              <i class="fas fa-paperclip"></i>-->
                          <!--                          </button>-->

                          <!--                          <button type="button"  class="botao_detalhes" data-toggle="modal" data-target="#editar--><?php //echo $relatorioDizimos['Id']; ?><!--">-->
                          <!--                              <i class="fas fa-pencil-alt"></i>-->
                          <!--                          </button>-->

                        <!--   <button type="button"  data-toggle="modal" data-toggle="modal" data-target="#excluir<?php echo $relatorioDizimos['id']; ?>" class=" view_data botao_detalhes3">
                              <i class="fas fa-trash-alt"></i>
                          </button>
                                                <button type="button"  data-toggle="modal" data-toggle="modal" data-target="#pdf--><?php //echo $relatorioDizimos['Id']; ?><!--" class="botao_detalhes4">-->
                          <!--                              <i class="far fa-file-pdf"></i>-->
                          <!--                          </button>-->

                          <!--                              <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal--><?php //echo $membro['id']; ?><!--">Visualizar</button>-->
                    <!--   </form>
                </td> -->
                
              </tr>

            <!-- Inicio Modal excluir-->
            <div class="modal" id="excluir<?php echo $relatorioDizimos['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            <h4 class="modal-title text-center" id="myModalLabel">Tem certeza que deseja excluir ?</h4>
                            <h4 class="modal-title text-center" id="myModalLabel"><?php echo $relatorioDizimos['id']; ?></h4>
                        </div>

                        <div class="modal-body">
                            <p class="form-control"> Essa é uma operação sem volta</p>
                            <form method="POST" action="relatorio.php">
                                <input type="text" name="id" hidden value="<?php echo $relatorioDizimos['id'] ?>">
                                <div class="modal-footer">
                                    <button type="button" class=" btn-secondary" data-dismiss="modal">fechar</button>
                                    <button type="submit" class=" btn-primary" name="excluir"> Excluir</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
  </div>

            <?php  endforeach?>

            
          </tbody>
          <tfoot>
            <tr>
            </tr>
          </tfoot>
        </table>
      
        
      </div>
    </div>
  </div>
  <!-- Column -->
  </div>

  <!-- ============================================================== -->
  <!-- Sales chart -->
  <!-- ============================================================== -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">

        <div class="card-body">
          <div class="d-md-flex align-items-center">
            <!--aqui pode ter um card-->
          </div>
          <div class="row">
            <!-- column -->
            <div class="col-lg-9">
              <!--AQUI PODE FICAR UM CARD-->
            </div>
            <div class="col-lg-3">
              <div class="row">
                <!--aqui fica um card a direita-->
              </div>
            </div>
          </div>
          <!-- column -->
        </div>
      </div>
    </div>
  </div>

  </div>

  </div>
  <?php include 'footer.php' ?>
</body>

</html>
