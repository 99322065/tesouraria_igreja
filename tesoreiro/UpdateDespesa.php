<?php
include '../Conexao/Conexao.php';
include 'Upload.php';

class UpdateDespesa extends Conexao {
    public function UpdadeDespesaComaAnexo($param){

        if ($arquivo=Upload::uploadImagem()){
            $id                 = $param['id'];
            $mes                = $param['mes'];
            $data               = $param['data'];
            $valor              = $param['valor'];
            $tipoDespesa        = $param['tipoDespesa'];
            $observacoes        = $param['observacoes'];

            $updateDadosDadespesaComAnexos = "UPDATE despesas SET mes=:mes,data=:data ,valor=:valor,tipoDespesa=:tipoDespesa ,observacoes=:observacoes,arquivo=:arquivo WHERE id=:id " ;
            $updateDadosDadespesaComAnexos = $this->db->prepare($updateDadosDadespesaComAnexos);
            $updateDadosDadespesaComAnexos->bindValue(':id',$id);
            $updateDadosDadespesaComAnexos->bindValue(':mes',$mes);
            $updateDadosDadespesaComAnexos->bindValue(':data',$data);
            $updateDadosDadespesaComAnexos->bindValue(':valor',$valor);
            $updateDadosDadespesaComAnexos->bindValue(':tipoDespesa',$tipoDespesa);
            $updateDadosDadespesaComAnexos->bindValue(':observacoes',$observacoes);
            $updateDadosDadespesaComAnexos->bindValue(':arquivo',$arquivo);
            $updateDadosDadespesaComAnexos->execute();
            if ( $updateDadosDadespesaComAnexos->rowCount() ){
                header("location: relatoriodespesas.php?update");
            }else{
                header("location: relatoriodespesas.php?naoCadastrado=ok");
            }

        }else{
            return Error::class;
        }

    }
}
