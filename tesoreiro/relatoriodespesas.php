<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include 'header.php';
include 'RelatorioGeral.php';
$relatorio = new RelatotioGeral();


include 'validarSeUsuarioEstarLogado.php';


?>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <?php
    include 'headerTop.php'
    ?>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?php
    include 'menuDash.php'
    ?>


    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 id="titulo" class="page-title">Relatorio de dizimos</h4>
                    <div class="ms-auto text-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="dashbord.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page"> <a href="despesas.php"> Nova Despesa</a> </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
</div>
</div>
<div class="bg-white container-xxl mt-4 ">
<div class=" d-flex relarotios"  >
        <div class="col-10 text" style="font-size:13px ;">
            <?php if ( isset($_GET['sucesso']) ) : ?>
                <div class="alert alert-warning alert-dismissible  show cor" role="alert">
                    <strong>Despesa Excluida</strong> .
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php elseif (isset($_GET['update']) ) : ?>
                <div class="alert alert alert-success alert-dismissible  show cor2" role="alert">
                    <strong>Sucesso</strong> Update feito com sucesso.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php elseif (isset($_GET['naoCadastrado']) ) : ?>
                <div class="alert alert-warning alert-dismissible  show cor2" role="alert">
                    <strong>Erro</strong> Tente novamente.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            <?php elseif (isset($_GET['urlvazia=ok']) ) : ?>
                <div class="alert alert-warning alert-dismissible  show cor2" role="alert">
                    <strong>Erro</strong> Você esta tentando acessa pela Url direto.
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>

            <?php endif  ?>

            <table border="0" cellspacing="3" cellpadding="5">

                </tbody></table>
            <table id="example" class="display" style="width:100%">
                <thead>
                <div>

                </div>
                <hr>
                <tr>
                    <th>id</th>
                    <th>Igreja</th>
                    <th>Mês</th>
                    <th>Data</th>
                    <th>Valor</th>
                    <th>TipoDespesa</th>
                    <th>Observacao</th>
                    <th>Arquivo</th>
                    <th>Detalhes</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($relatorio->relatoriodedespesa() as $despesa) : ?>
                    <tr>
                        <?php 
                            $mes = $despesa['mes'];
                            $data = $despesa['data'];
                        ?>
                        <?php $ext = strtolower(substr($despesa['arquivo'],-4));  ?>
                        <td><?php echo $despesa['id_despesa'] ?></td>
                        <td><?php echo $despesa['nome_igreja'] ?></td>
                        <td><?php echo date('m-Y',   strtotime($mes));?></td>
                        <td><?php echo date('d-m-Y', strtotime($data));?></td>
                        <td><?php echo $despesa['valor'] ?></td>
                        <td><?php echo $despesa['tipoDespesa'] ?></td>
                        <td><?php echo $despesa['observacoes'] ?></td>
                        <td>  
                            <img class="imgRelatorio"   src="../upload/despesa/<?php echo $despesa['arquivo']?> " data-toggle="modal" data-target="#modalVisualizar<?php echo $despesa['id_despesa']; ?>"> </td>
                        <td>
                        <form class="form_bt" method="GET" action="relatoriodespesas.php">
                            <button  type="button"  class="view_data botao_detalhes2" data-toggle="modal" data-target="#modalVisualizar<?php echo $despesa['id_despesa']; ?>">
                                <i class="fas fa-paperclip"></i>
                            </button>

                            <button type="button"  class="botao_detalhes" data-toggle="modal" data-target="#editar<?php echo $despesa['id_despesa']; ?>">
                                <i class="fas fa-pencil-alt"></i>
                            </button>

                            <button type="button"  data-toggle="modal" data-toggle="modal" data-target="#excluir<?php echo $despesa['id_despesa']; ?>"" class="botao_detalhes3">
                                <i class="fas fa-trash-alt"></i>
                            </button>

<!--                            <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal--><?php //echo $despesa['id_despesa']; ?><!--">Visualizar</button>-->
                        </form>
                        </td>
                    </tr>

                    <!-- Inicio Modal -->
                    <div class="modal " id="modalVisualizar<?php echo $despesa['id_despesa']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                    <h4 class="modal-title text-center" id="myModalLabel"><?php echo $despesa['nome_igreja']; ?></h4>
                                    <h4 class="modal-title text-center" id="myModalLabel"><?php echo $despesa['id_despesa']; ?></h4>

                                </div>
                                <div class="modal-body">
                                    <div> <label class=" fas " >Departamento: </label> <?php echo $despesa['tipoDespesa']; ?>    <hr> </div>
                                    <div> <label class=" fas " >Data despesa: </label> <?php echo  $despesa['data']; ?>          <hr> </div>
                                    <div> <label class=" fas " >Mês: </label>          <?php echo   $despesa['mes']; ?>          <hr> </div>
                                    <div> <label class=" fas " >Valor: </label>        <?php echo  $despesa['valor']; ?>         <hr> </div>
                                    <div> <label class=" fas " >Anexo da despesa: </label> <br><img class="imdAnexo" src="../upload/despesa/<?php echo $despesa['arquivo']?>"<hr> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fim Modal -->


                    <!-- Inicio Modal excluir-->
                    <div class="modal " id="excluir<?php echo $despesa['id_despesa']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                    <h4 class="modal-title text-center" id="myModalLabel">Tem certeza que deseja excluir ?</h4>
                                    <h4 class="modal-title text-center" id="myModalLabel"><?php echo $despesa['id_despesa']; ?></h4>
                                </div>

                                <div class="modal-body">
                                    <p class="form-control"> Essa é uma operação sem volta</p>
                                    <form method="POST" action="excluirDespesas.php">
                                        <input type="text" name="id" hidden value="<?php echo $despesa['id_despesa'] ?>">
                                    <div class="modal-footer">
                                        <button type="button" class="btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn-primary" name="excluir"> Excluir</button>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fim Modal -->


        <!-- Inicio Modal Editar-->
        <div class="modal " id="editar<?php echo $despesa['id_despesa']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title text-center" id="myModalLabel"><?php echo $despesa['nome_igreja']; ?></h4>
                        <h4 class="modal-title text-center" id="myModalLabel"><?php echo $despesa['id_despesa']; ?></h4>

                    </div>
                        <div class="modal-body">
                        <form action="validaUpdate.php" method="post" enctype="multipart/form-data">
                            <div class="m-2">
                                <label class="">Id despesa</label>
                                <input type="text" disabled  class="modal-title text-center align-content-center float-end"  value="<?php echo $despesa['id_despesa']; ?> ">
                                <input type="text" hidden name="id"   class="modal-title text-center align-content-center float-end"  value="<?php echo $despesa['id_despesa']; ?> ">
                            </div>
                            <div class="m-2">
                                <label class="">Tipo deespesa</label>
                                <input type="text" name="tipoDespesa" class="modal-title text-center align-content-center float-end"  value="<?php echo $despesa['tipoDespesa']; ?> ">
                            </div>
                            <div class="m-2">
                                <label class="">Data da despesa</label>
                                <input type="date" name="data" class="modal-title text-center align-content-center float-end"  value="<?php echo $despesa['data']; ?> ">
                            </div>
                            <div class="m-2">
                                <label class="">Mẽs da despesa</label>
                                <input type="month" name="mes" class="modal-title text-center align-content-center float-end"  value="<?php echo $despesa['mes']; ?> ">
                            </div>
                            <div class="m-2">
                                <label class="">Valor da despesa</label>
                                <input type="text" name="valor" class="modal-title text-center align-content-center float-end"  value="<?php echo $despesa['valor']; ?> ">
                            </div>
                            <div class="m-2">
                                <label class="">Obesvações</label>
                                <input type="text" name="observacoes" class="modal-title text-center align-content-center float-end"  value="<?php echo $despesa['observacoes']; ?> ">
                            </div>


                            <div class="m-2">
                                <label class="">Seleciono novo comprovante</label>
                                <input type="file"  name="arquivo" accept="image/*" class="modal-title text-center align-content-center float-end"  value="<?php echo $despesa['arquivo']; ?> ">
                                <input type="text" hidden name="nomeAntigoAnexo" class="modal-title text-center align-content-center float-end"  value="<?php echo $despesa['arquivo']; ?> ">
                            </div>

                            <div class="m-2">  <label class=" fas " >Anexo da despesa: </label> <br><img class="imdAnexo" src="../upload/despesa/<?php echo $despesa['arquivo']?>"<hr> </div>
                       <button type="submit" value="update" class="btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <!-- Fim Modal -->

                <?php endforeach ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<!-- Column -->
</div>
<!-- ============================================================== -->
<!-- Sales chart -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12">
        <div class="card">

            <div class="card-body">
                <div class="d-md-flex align-items-center">
                    <!--aqui pode ter um card-->
                </div>
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-9">
                        <!--AQUI PODE FICAR UM CARD-->
                    </div>
                    <div class="col-lg-3">
                        <div class="row">
                            <!--aqui fica um card a direita-->
                        </div>
                    </div>
                </div>
                <!-- column -->
            </div>
        </div>
    </div>
</div>

</div>

</div>
<?php include 'footer.php' ?>
</body>

</html>
