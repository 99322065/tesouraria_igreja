<?php
// include '../Conexao/Conexao.php';
require 'PerfilFinanceiro.php';

class Membro extends PerfilFinanceiro
{

    private $tipo;

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo): void
    {
        $this->tipo = $tipo;
    }


    public function cadastarNovoMembro($param){
        $nome_membro        = $param['nome_membro'];
        $email_dizimista    = $param['email_dizimista'];
        $cidade             = $param['cidade'];
        $barrio             = $param['barrio'];
        $endereco           = $param['endereco'];
        $telefone           = $param['telefone'];
        $batismo_agua       = $param['batismo_agua'];
        $data_nascimento       = $param['data_nascimento'];
        $cargo              = $param['cargo'];
        $situacao           = $param['situacao'];
        $fk_igreja          = $param['fk_igreja'];
        $data_batismo_espirito_santo =$param['data_batismo_espirito_santo'];
        $sexo =$param['sexo'];

        if($data_batismo_espirito_santo == '')
            $data_batismo_espirito_santo = null;

        if($data_nascimento =='')
            $data_nascimento = null;
        
        if($batismo_agua =='')
            $batismo_agua = null;
        

        $cadasdtarNovosMembrosAssembleiaDeus = " INSERT INTO membros (nome_membro,email_dizimista,cidade,barrio,endereco,telefone,batismo_agua,data_nascimento,cargo,situacao,fk_igreja,data_batismo_espirito_santo,sexo) 
        values(:nome_membro,:email_dizimista,:cidade,:barrio,:endereco,:telefone,:batismo_agua,:data_nascimento,:cargo,:situacao,:fk_igreja,:data_batismo_espirito_santo,:sexo)";

        $cadasdtarNovosMembrosAssembleiaDeus = $this->db->prepare($cadasdtarNovosMembrosAssembleiaDeus);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':nome_membro', $nome_membro);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':email_dizimista', $email_dizimista);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':cidade', $cidade);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':barrio', $barrio);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':endereco', $endereco);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':telefone', $telefone);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':batismo_agua', $batismo_agua);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':data_nascimento', $data_nascimento);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':cargo', $cargo);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':situacao', $situacao);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':fk_igreja', $fk_igreja);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':data_batismo_espirito_santo', $data_batismo_espirito_santo);
        $cadasdtarNovosMembrosAssembleiaDeus->bindValue(':sexo', $sexo);
        $cadasdtarNovosMembrosAssembleiaDeus->execute();
        if ($cadasdtarNovosMembrosAssembleiaDeus->rowCount() > 0){
            header("location: cadastroMembro.php?sucesso=ok");
        }else{
            header("location: cadastroMembro.php?naoCadastrado=ok");
        }
    }

    public function cadastraNovoTipo(){

        $tipo = $this->getTipo();

        $cadastraNovoTipo = " INSERT INTO tipo (tipo) values(:tipo)";
        $cadastraNovoTipo = $this->db->prepare($cadastraNovoTipo);
        $cadastraNovoTipo->bindValue(':tipo', $tipo);
        $cadastraNovoTipo->execute();
        if ($cadastraNovoTipo->rowCount() > 0){
            header("location: despesas.php?sucesso=ok");
        }else{
            header("location: despesas.php?naoCadastrado=ok");
        }
    }

    public function pegarTipo(){
        $pegatTipo = "SELECT * FROM tipo ";
        $pegatTipo = $this->db->prepare($pegatTipo);
        $pegatTipo->execute();
        return $pegatTipo->fetchAll();
    }


    public function pegarMembro(){
        $selecionarMembroDaigreja = "
        SELECT 
               membros.id               AS Id,
               nome_membro              as Nome_membro,
               email_dizimista          as Email_dizimista,
               membros.cidade           as Cidade ,
               membros.barrio           as Barrio,
               membros.endereco         as Endereco,
               membros.telefone         as Telefone,
               batismo_agua             as Batismo_agua,
               data_batismo_espirito_santo             as data_batismo_espirito_santo,
               data_nascimento as data_nascimento,
               tipo.tipo                     as Cargo,
               situacao                 as Situacao,
               fk_igreja                as id_igreja,
               igrejas.id               as id_i,
               igrejas.nome_igreja      as igreja
        FROM membros 
         left join igrejas ON igrejas.id = membros.fk_igreja
         left join tipo ON tipo.id = membros.cargo
        ";
        $selecionarMembroDaigreja = $this->db->prepare($selecionarMembroDaigreja);
        $selecionarMembroDaigreja->execute();
        return $selecionarMembroDaigreja->fetchAll();

    }

    public function updateMembro($param)
    {
        $nome_membro =      $param['nome_membro'];
        $id =               $param['id'];
        $telefone =         $param['telefone'];
        $email_dizimista =  $param['email_dizimista'];
        $cidade =           $param['cidade'];
        $barrio =           $param['barrio'];
        $endereco =         $param['endereco'];
        $batismo_agua =     $param['batismo_agua'];
        $cargo =            $param['cargo'];
        $situacao =         $param['situacao'];


        $updateIfomacoesMembro = "
                UPDATE membros SET 
                   nome_membro = :nome_membro ,
                   email_dizimista =:email_dizimista ,
                   cidade = :cidade,
                   barrio =:barrio,
                   endereco=:endereco,
                   telefone=:telefone,
                   batismo_agua=:batismo_agua,cargo=:cargo,situacao=:situacao
                WHERE id=:id ";
        $updateIfomacoesMembro = $this->db->prepare($updateIfomacoesMembro);
        $updateIfomacoesMembro->bindValue(':nome_membro', $nome_membro);
        $updateIfomacoesMembro->bindValue(':id', $id);
        $updateIfomacoesMembro->bindValue(':email_dizimista', $email_dizimista);
        $updateIfomacoesMembro->bindValue(':cidade', $cidade);
        $updateIfomacoesMembro->bindValue(':barrio', $barrio);
        $updateIfomacoesMembro->bindValue(':endereco', $endereco);
        $updateIfomacoesMembro->bindValue(':telefone', $telefone);
        $updateIfomacoesMembro->bindValue(':batismo_agua', $batismo_agua);
        $updateIfomacoesMembro->bindValue(':cargo', $cargo);
        $updateIfomacoesMembro->bindValue(':situacao', $situacao);

        $updateIfomacoesMembro->execute();
        if ($updateIfomacoesMembro->rowCount() > 0)
        {
            header("location: ralatorioMembros.php?update=ok");
        }else{
            header("location: ralatorioMembros.php?naoCadastrado=ok");
        }
    }

    public function deletaMembro($param)
    {
//           var_dump($param);die();
        $id = $param['id'];

        $excluirDespesa = "DELETE FROM membros WHERE id=:id";
        $excluirDespesa = $this->db->prepare($excluirDespesa);
        $excluirDespesa->bindValue(':id',$param['id']);
        $excluirDespesa->execute();
        if ($excluirDespesa->rowCount() > 0){
            header("location: ralatorioMembros.php?sucesso=ok");
        }else{
            header("location: ralatorioMembros.php?naoCadastrado=ok");
        }
    }

}