<!DOCTYPE html>
<html dir="ltr" lang="en">
<?php
    include 'header.php';

include 'validarSeUsuarioEstarLogado.php';

?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

        <?php
        include 'headerTop.php'
        ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php
        include 'menuDash.php'
        ?>

        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Assembleia de Deus  </h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Library
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid tamanhodoInpunt">
                <div class="row">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body ">
                                    <?php  if( isset($_GET['sucesso']) ):?>
                                    <div class="alert alert-success alert-dismissible  show cor" role="alert">
                                        <strong>Sucesso</strong> As informaçoes do formulario foram salvas com sucesso.
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                                            aria-label="Close"></button>
                                    </div>
                                    <?php elseif( isset($_GET['naoCadastrado'] ) ):?>
                                    <div class="alert alert-warning alert-dismissible  show cor2" role="alert">
                                        <strong>Erro</strong> Tente novamente .
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                                            aria-label="Close"></button>
                                    </div>
                                    <?php endif  ?>

                                    <form action="validaFormularioIgreja.php" method="post" autocomplete="off">
                                        <div id="bt" class="form-row">
                                            <label>
                                                <h4>Cadastro Igreja</h4>
                                            </label>
                                            <div class="col lista">
                                                <input type="text" name="dirigente" required class="form-control input"
                                                    placeholder="Dirigente local">
                                            </div>
                                            <div class="col lista">
                                                <input type="text" name="nome_igreja" required class="form-control input"
                                                    placeholder="Nome da igreja">
                                            </div>
                                            <div class="col lista">
                                                <input type="text" name="cidade" required class="form-control input"
                                                    placeholder="Cidade">
                                            </div>
                                            <div class="col lista">
                                                <input type="text" name="barrio" required class="form-control input"
                                                    placeholder="Barrio">
                                            </div>
                                            <div class="col lista">
                                                <input type="text" name="endereco" required class="form-control input"
                                                    placeholder="Endereço">
                                            </div>
                                            <button type="submit" name="cadastrar"
                                                class="botao">Cadastrar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- ============================================================== -->
    <!-- Sales chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-md-flex align-items-center">
                        <!--aqui pode ter um card-->
                    </div>
                    <div class="row">
                        <!-- column -->
                        <div class="col-lg-9">
                            <!--AQUI PODE FICAR UM CARD-->
                        </div>
                        <div class="col-lg-3">
                            <div class="row">
                                <!--aqui fica um card a direita-->
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                </div>
            </div>
        </div>
    </div>
    </div>

    <?php include 'footer.php' ?>
</body>

</html>