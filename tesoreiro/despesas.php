<?php
    include 'Query.php';
    $query = new Query();

include 'validarSeUsuarioEstarLogado.php';

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include 'header.php' ?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <?php
        include 'headerTop.php'
        ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php
        include 'menuDash.php'
        ?>

        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 id="titulo" class="page-title">Assembleia de Deus AD53 </h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Library
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid tamanhodoInpunt">
                <div class="row">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body ">
                                    <?php if ( isset($_GET['sucesso']) ) : ?>
                                        <div class="alert alert-success alert-dismissible  show " role="alert">
                                            <strong>Sucesso</strong> As informaçoes do formulario foram salvas com sucesso.
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    <?php elseif (isset($_GET['naoCadastrado']) ) : ?>
                                        <div class="alert alert-warning alert-dismissible  show cor2" role="alert">
                                            <strong>Erro</strong> Tente novamente.
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    <?php elseif (isset($_GET['extessaoErrada']) ) : ?>
                                        <div class="alert alert-danger  alert-dismissible  show" role="alert">
                                            <strong>ERRO</strong> Você selecionou um arquivo não suportado.
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    <?php endif  ?>

                                    <form class="class="selectpicker" data-width="100%" action="validarFormularioDespesas.php" method="post" autocomplete="off"  enctype="multipart/form-data">
                                        <div id="bt" class="form-row">
                                            <label>
                                                <h4>Lançar despesas</h4>
                                            </label>
                                            <div id="box" class="col lista">
                                                <select class="selectpicker" data-width="100%" data-size="10" data-show-subtext="true" data-live-search="true" name="fk_igreja" required>
                                                    <?php foreach ($query->getSelect('igrejas') as $igreja):  ?>
                                                        <option value='<?php echo $igreja['id'] ?>'><?php echo $igreja['nome_igreja'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            
                                            <div class="col lista">
                                                <input type="month" name="mes" required class="form-control input" placeholder="mes">
                                            </div>
                                            <div class="col lista">
                                                <input type="date" name="data" required class="form-control input" placeholder="data">
                                            </div>
                                            
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text input">Valor R$: </span>
                                                </div>
                                                <input type="text" name="valor" class="form-control input" aria-label="Amount (to the nearest dollar)">
                                            </div>
                                            <div class="col lista form-inline " style="position: relative;">
                                            
                                                <select class="selectpicker" data-width="90.3%" data-size="10" data-show-subtext="true" data-live-search="true" name="tipoDespesa" required>
                                                <?php foreach ($query->getSelect('tipo') as $tipo):  ?>
                                                        <option value='<?php echo $tipo['id'] ?>'><?php echo $tipo['tipo'] ?></option>
                                                    <?php endforeach ?>
                                                </select>

                                                <button class="botao_detalhes" type="button"  data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                                    <i class="mdi mdi-hospital"></i>
                                                </button>
                                                
                                            </div>
                                                                                        
                                            <div class="col lista">
                                                <input type="text" name="observacoes" required class="form-control input" placeholder="Observações">
                                            </div>
                                            <div class="col lista">
                                                <input type="file" accept="image/*" name="arquivo"  class="form-control input" >
                                            </div>
                                            <!-- input valor
                                                <div class="col lista">
                                                <input id="valor" type="number" name="valor" required class="form-control" placeholder="valor">
                                            </div>
                                            -->
                                            <button type="submit" id="teste" name="cadastrar" class="botao">Lançar</button>
                                        </div>
                                    </form>



                                         <!-- Modal -->
                                         <div class="modal" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="staticBackdropLabel">Cadasrea Novo tipo de despesas</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">

                                                           <form class="form-inline" method="post" action="validaFormularioMembro.php">
                                                                <div class="form-group mx-sm-3 mb-2">
                                                                    <label for="staticEmail2" class="form-control input">Novo tipo de despesas </label>
                                                                    <input type="text" class="form-control input" name="tipo" placeholder="tipo da despesa">
                                                                </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class=" btn-secondary" data-bs-dismiss="modal">fechar</button>
                                                        <button type="submit" class="btn-primary" value="salvar" name="salvar">Salvar</button>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- ============================================================== -->
    <!-- Sales chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-md-flex align-items-center">
                        <!--aqui pode ter um card-->
                    </div>
                    <div class="row">
                        <!-- column -->
                        <div class="col-lg-9">
                            <!--AQUI PODE FICAR UM CARD-->
                        </div>
                        <div class="col-lg-3">
                            <div class="row">
                                <!--aqui fica um card a direita-->
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                </div>
            </div>
        </div>
    </div>
    </div>

   <?php include 'footer.php'?>
</body>

</html>