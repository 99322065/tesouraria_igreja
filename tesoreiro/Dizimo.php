<?php

use Mpdf\Tag\Select;

include 'CadastroIgreja.php';

class Dizimo extends CadastroIgreja
{
    public function cadastarDizimo($param){
        
        if($this->evitarDuplicidaded($param) == true ){
            header("location: financeiro.php?duplicado=ok");exit;
        };

        $fk_dizimista   = $param['fk_dizimista'];
        $mes            = $param['mes'];
        $data_pagamento = $param['data'];
        $valor        = $param['valor'];
        $fk_igreja      = $this->pegarDizimosDaigrejaQueOdizimistaEcadastrado($fk_dizimista);
          
        $cadastramentoDizimo = "INSERT INTO dizimimos (fk_igreja,fk_dizimista,mes,data,valor) values (:fk_igreja,:fk_dizimista,:mes,:data,:valor)";
        $cadastramentoDizimo = $this->db->prepare($cadastramentoDizimo);
        $cadastramentoDizimo->bindValue(':fk_igreja',$fk_igreja);
        $cadastramentoDizimo->bindValue(':fk_dizimista',$fk_dizimista);
        $cadastramentoDizimo->bindValue(':mes',$mes);
        $cadastramentoDizimo->bindValue(':data',$data_pagamento);
        $cadastramentoDizimo->bindValue(':valor',$valor);
        $cadastramentoDizimo->execute();
            if( $cadastramentoDizimo->rowCount() > 0 ){
                header("location: financeiro.php?sucesso=ok");
            }else{
                header("location: financeiro.php?naoCadastrado=ok");
            }
    }

    public function pegarDizimosDaigrejaQueOdizimistaEcadastrado($fk_dizimista){
        $pegarIgrejaQueOdizimistaEcadastrado = "SELECT fk_igreja FROM membros where id = $fk_dizimista ";
        $pegarIgrejaQueOdizimistaEcadastrado= $this->db->prepare($pegarIgrejaQueOdizimistaEcadastrado);
        $pegarIgrejaQueOdizimistaEcadastrado->execute();
        return $pegarIgrejaQueOdizimistaEcadastrado->fetch()[0];
    }


    public function evitarDuplicidaded($param){

        $fk_dizimista   = $param['fk_dizimista'];
        $mes            = $param['mes'];
        $data_pagamento = $param['data'];
        $valor          = $param['valor'];
    
        $fazerConsulta = "SELECT * from dizimimos WHERE fk_dizimista = '$fk_dizimista' and data = '$data_pagamento' and valor = '$valor' and mes = '$mes' ";
        $fazerConsulta = $this->db->prepare($fazerConsulta);
        $fazerConsulta->execute();

        if($fazerConsulta->rowCount() > 0){
            return true;
        }else {
            return false;
        }
    } 



}




