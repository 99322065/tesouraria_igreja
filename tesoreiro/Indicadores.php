<?php
include 'CadastroIgreja.php';


class Indicadores extends CadastroIgreja
{
    private  $de;
    private $ate;

    /**
     * @return mixed
     */
    public function getDe()
    {
        return $this->de;
    }

    /**
     * @param mixed $de
     * @return Indicadores
     */
    public function setDe($de)
    {
        $this->de = $de;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAte()
    {
        return $this->ate;
    }

    /**
     * @param mixed $ate
     * @return Indicadores
     */
    public function setAte($ate)
    {
        $this->ate = $ate;
        return $this;
    }

    //  aqui é a porcentagem que é envaido pra CEADEP
    const PORCENTAGEM_CEADEP = 0.10;

     /*
    * Membros : quantidade de membros , mas pode pega a quantidade de qual quer tabela .
    */
    public function quantidadesEmUmaDeterMinadaTabela($tabela)
    {
        
            return $this->selectFiltrosDasboardeDequatidadesPeloId($tabela, $this->getDe(), $this->getAte());
        
    }


    // pega quantidade de registro em  qual quer tabela pelo (id) 
    public function selectFiltrosDasboardeDequatidadesPeloId($tabela){

        $quantidadeNaTabela = " SELECT * FROM $tabela  ";
        $quantidadeNaTabela = $this->db->prepare($quantidadeNaTabela);
        $quantidadeNaTabela->execute();
        $quantidade = $quantidadeNaTabela->fetchAll();
        $total = array_column($quantidade,'id');
        $contarMen = array_count_values($total);

        $contar = 0;
        
        foreach ($contarMen as $d){
            $contar +=  $d;
        }
        
        return $contar;
    }

    /*
    * pegar o valor de qual quer tabela, basta passar o nome da tabela.
    * OBS: a tabela deve ter o campo "valor" 
    */
    public function valor($tabela)
    {
        $valorgeral = $this->getSelect("$tabela");
        $total = array_sum(array_column($valorgeral,'valor'));

        return $total;
    }

    /*
    * faz a soma do Caixa da igreja. 
    * Não esta sendo usado
    */
    public function caixa (){
        $saida = $this->valor('despesas');
        $entrada = $this->valor('dizimimos');
        $ofertas = $this->valor('ofertas');

        $total  = ($entrada + $ofertas) + $saida;
        return $total;
    }

    /*
    *  faz o calculo do valor do caixa 
    */
    public function caixaComFiltro (){
        $saida =   $this->filtrosIndicadores('despesas');
        $entrada = $this->filtrosIndicadores('dizimimos');
        $ofertas = $this->filtrosIndicadores('ofertas');

        $total  = ($entrada + $ofertas) + $saida;
        return $total;
    }

    /*
    * reesultado dos filtros dos indicadores
    */
    public function filtrosIndicadores($tabela){
        if ($this->getDe() != null) {
            $resultado_filtro = $this->selectFiltrosDasboarde($tabela, $this->getDe(), $this->getAte());

        }else{
            $resultado_filtro = $this->valor($tabela);
        }

        return $resultado_filtro;
    }
    /*
    *  Faz o filtros nas tabelas no Banco de dados
    */
    public function selectFiltrosDasboarde($tabela, $de, $ate){

        $selecionarIgrejasLocais = " SELECT * FROM $tabela where data >= '$de' and data <= '$ate'  ";
        $selecionarIgrejasLocais = $this->db->prepare($selecionarIgrejasLocais);
        $selecionarIgrejasLocais->execute();
        $valor = $selecionarIgrejasLocais->fetchAll();
        $total = array_sum(array_column($valor,'valor'));
        return $total;

    }


    //  calcular os 10% da ceadepe com o valor do caixa a penas 
    public function CalculoCeadepeValorCaixa()
    {
        $pegarValoresCixa = $this->caixaComFiltro()  / (100 * self::PORCENTAGEM_CEADEP);
        return $pegarValoresCixa;
    }


    //  calcular os 10% da ceadepe com o valor do caixa com toda a despesas  
    public function CalculoCeadepeValorComTodasAsReceitas(){
       
        // $saida =   $this->filtrosIndicadores('despesas');
        $entrada = $this->filtrosIndicadores('dizimimos');
        $ofertas = $this->filtrosIndicadores('ofertas');

        $total  = ($entrada + $ofertas) / (100 * self::PORCENTAGEM_CEADEP);

        return $total;
    }

}
