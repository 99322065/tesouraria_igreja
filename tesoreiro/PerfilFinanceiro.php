<?php

include ('Indicadores.php');

class PerfilFinanceiro extends Indicadores
{

    public function perfilFinanceiroDizimista($id)
    {
    
        $selecPerfilFinanceiroDizimistas = 
        "SELECT 
            nome_membro AS membro 
            ,t.tipo as cargo ,
            d.data, 
            SUM(d.valor) as valor 
        FROM membros m 
            inner join dizimimos d  on m.id = d.fk_dizimista 
            INNER JOIN tipo t on t.id  = m.cargo 
        WHERE m.id = '$id'  GROUP  BY  nome_membro, d.data,t.tipo  limit 12";

        $selecPerfilFinanceiroDizimistas = $this->db->prepare($selecPerfilFinanceiroDizimistas);
        $selecPerfilFinanceiroDizimistas->execute();
        $selecPerfilFinanceiroDizimistas = $selecPerfilFinanceiroDizimistas->fetchAll();

        return $selecPerfilFinanceiroDizimistas;
    }


    public function perfilFinanceiroDizimistaValorTotal($id)
    {
    
        $selecPerfilFinanceiroDizimistas = 
        "SELECT 
            SUM(d.valor) as valor 
        FROM membros m 
            inner join dizimimos d  on m.id = d.fk_dizimista 
            INNER JOIN tipo t on t.id  = m.cargo 
        WHERE m.id = '$id'  GROUP  BY  nome_membro, d.data,t.tipo ";

        $selecPerfilFinanceiroDizimistas = $this->db->prepare($selecPerfilFinanceiroDizimistas);
        $selecPerfilFinanceiroDizimistas->execute();
        $selecPerfilFinanceiroDizimistas = $selecPerfilFinanceiroDizimistas->fetchAll();

        $total = array_sum(array_column($selecPerfilFinanceiroDizimistas,'valor'));

        return $total;
    }
}