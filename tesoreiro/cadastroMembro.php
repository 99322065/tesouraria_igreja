
<?php
    include 'Query.php';
    $query = new Query();

include 'validarSeUsuarioEstarLogado.php';

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include 'header.php'?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php
        include 'headerTop.php'
        ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php
        include 'menuDash.php'
        ?>

        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Assembleia de Deus/castro de membros</h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Library
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid tamanhodoInpunt">
                <div class="row">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body ">
                                    <?php if ( isset($_GET['sucesso']) ) : ?>
                                        <div class="alert alert-success alert-dismissible  show " role="alert">
                                            <strong>Sucesso</strong> As informaçoes do formulario foram salvas com sucesso.
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    <?php elseif (isset($_GET['naoCadastrado']) ) : ?>
                                        <div class="alert alert-warning alert-dismissible  show cor2" role="alert">
                                            <strong>Erro</strong> Tente novamente.
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    <?php elseif (isset($_GET['vazio']) ) : ?>
                                        <div class="alert alert-danger  alert-dismissible  show" role="alert">
                                            <strong>ERRO</strong> Você precisa preencher os campos.
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    <?php endif  ?>
                                    <form action="validaFormularioMembro.php" method="post" autocomplete="off">
                                        <div id="bt" class="form-row">
                                            <label>
                                                <h4 id="titulo"> </h4>
                                            </label>
                                            <div class="col lista input-group">
                                                <div class="input-group-prepend col" >
                                                    <input type="text" name="nome_membro" required class="form-control input" placeholder="Nome completo">
                                                </div>

                                                <div class="input-group-prepend ms-3 col"  >
                                                    <input type="text" name="email_dizimista"  class="form-control input" placeholder="Email">
                                                </div>
                                            </div>

                                            <div class="col lista input-group">

                                                <div class="input-group-prepend col" >
                                                    <input type="text" name="cidade"  class="form-control input" placeholder="Cidade">
                                                </div>

                                                <div class="input-group-prepend ms-3 col"  >
                                                    <input type="text" name="barrio"  class="form-control input" placeholder="Bairro">
                                                </div>

                                                <div class="input-group-prepend ms-3 col"  >
                                                    <input type="text" name="endereco"  class="form-control input" placeholder="Endereço">
                                                </div>

                                            </div>


                                            <div class="col lista input-group">

                                                <div class="input-group-prepend col " >
                                                    <input type="tel" name="telefone"  class="form-control input" placeholder="Telefone">
                                                </div>

                                                <div class="input-group-prepend col ms-3" >
                                                    <select class="selectpicker" data-width="100%" data-size="10" data-show-subtext="true" data-live-search="true" name="sexo">
                                                        <option selected>selecione um sexo</option>
                                                        <option value="masculino">Masculino</option>
                                                        <option value="feminino">Feminino</option>
                                                    </select>
                                                </div>

                                            </div>


                                            <div class="input-group mb-3 col">

                                                <div class="input-group-prepend" >
                                                    <span class="input-group-text input" name="batismo_agua">Data batismo água </span>
                                                </div>
                                                <input type="date" name="batismo_agua" class="form-control input" aria-label="Amount (to the nearest dollar)">


                                                <div class="input-group-prepend ms-4">
                                                    <span class="input-group-text input" name="data_nascimento">Data de nascimento </span>
                                                </div>
                                                <input type="date" name="data_nascimento" class="form-control input" aria-label="Amount (to the nearest dollar)">


                                                <div class="input-group-prepend ms-4">
                                                    <span class="input-group-text input" name="data_batismo_espirito_santo">Data Batismo espirito santo </span>
                                                </div>
                                                <input type="date" name="data_batismo_espirito_santo" class="form-control input" aria-label="Amount (to the nearest dollar)">

                                            </div>
                                            <div class="input-group">

                                                <div class="input-group-prepend col " >

                                                    <select class="selectpicker" data-width="100%" data-size="10" data-show-subtext="true" data-live-search="true" name="cargo" >
                                                        <?php foreach ($query->getSelect('tipo') as $tipo):  ?>
                                                            <option value='<?php echo $tipo['id'] ?>'><?php echo $tipo['tipo'] ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <button type="button" class="botao_detalhes mt-1 ms-2" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                                    <i class="mdi mdi-hospital"></i>
                                                </button>

                                                <div class="input-group-prepend col ms-3" >
                                                    <select class="selectpicker" data-width="100%" data-size="10" data-show-subtext="true" data-live-search="true" name="situacao" >
                                                        <option value="ativo">Ativo</option>
                                                        <option value="afastado">Afastado</option>
                                                        <option value="transferido">Transferido</option>
                                                    </select>
                                                </div>

                                                <div class="input-group-prepend col ms-3" >
                                                    <select class="selectpicker" data-width="100%" data-size="10" data-show-subtext="true" data-live-search="true" name="fk_igreja" >
                                                        <?php foreach ($query->getSelect('igrejas') as $igreja):  ?>
                                                            <option value='<?php echo $igreja['id'] ?>'><?php echo $igreja['nome_igreja'] ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="" style="margin-left: 93%">
                                                <button type="submit" name="cadastrar" class="btn-custom btn-primary">Cadastrar</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div class="modal" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Cadasrea Novo tipo</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                            <form class="form-inline" method="post" action="validaFormularioMembro.php">
                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="staticEmail2" class="form-control input">Tipo</label>
                                    <input type="text" class="form-control input" name="tipo" placeholder="cargo">
                                </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class=" btn-secondary" data-bs-dismiss="modal">fechar</button>
                        <button type="submit" class="btn-primary" value="salvar" name="salvar">Salvar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Column -->
    </div>
    <!-- ============================================================== -->
    <!-- Sales chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-md-flex align-items-center">
                        <!--aqui pode ter um card-->
                    </div>
                    <div class="row">
                        <!-- column -->
                        <div class="col-lg-9">
                            <!--AQUI PODE FICAR UM CARD-->
                        </div>
                        <div class="col-lg-3">
                            <div class="row">
                                <!--aqui fica um card a direita-->
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                </div>
            </div>
        </div>
    </div>
    </div>

   <?php include 'footer.php' ?>
</body>

</html>