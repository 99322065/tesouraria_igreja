<!DOCTYPE html>
<?php session_start();
$usuarioLOgado = $_SESSION['emailLogado'];
if (!isset($usuarioLOgado)){
    header("location: PaginaDeLogin.php?usuarioNaoLogado");
}

include 'Indicadores.php';
$indicarores = new Indicadores();
$indicarores->setDe($_POST['de']);
$indicarores->setAte($_POST['ate']);
?>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta
            name="keywords"
            content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Matrix lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Matrix admin lite design, Matrix admin lite dashboard bootstrap 5 dashboard template"
    />
    <meta
            name="description"
            content="Matrix Admin Lite Free Version is powerful and clean admin dashboard template, inpired from Bootstrap Framework"
    />
    <meta name="robots" content="noindex,nofollow" />
    <title>Assembleia de Deus</title>
    <!-- Favicon icon -->
    <link
            class="logo-icon ps-2"
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="https://www.serfautec.com.br/images/igraja-serfautec.jpg"
    />
    <!-- Custom CSS -->
    <link href="../front-end/Views/assets/libs/flot/css/float-chart.css" rel="stylesheet" />
    <link href="../front-end/Views/dist/css/style.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <![endif]-->
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div
        id="main-wrapper"
        data-layout="vertical"
        data-navbarbg="skin5"
        data-sidebartype="full"
        data-sidebar-position="absolute"
        data-header-position="absolute"
        data-boxed-layout="full"
>
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?php
    include 'headerTop.php'
    ?>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?php
    include 'menuDash.php'
    ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
<!--        <div class="page-breadcrumb">-->
<!--            <div class="row">-->
<!--                <div class="col-12 d-flex no-block align-items-center">-->
<!--                    <h4 class="page-title">Indicadores geral</h4>-->
<!--                    <div class="ms-auto text-end">-->
<!--                        <nav aria-label="breadcrumb">-->
<!--                            <ol class="breadcrumb">-->
<!--                                <li class="breadcrumb-item"><a href="#">Home</a></li>-->
<!--                                <li class="breadcrumb-item active" aria-current="page">-->
<!--                                    Library-->
<!--                                </li>-->
<!--                            </ol>-->
<!--                        </nav>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">

                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Indicaroes </h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">

                                    <li class="breadcrumb-item active" aria-current="page">
                                        <form method="post" action="dashbord.php">
                                            <div class="row">

                                                <div class="col mt-4">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="inputGroupPrepend">De</span>
                                                        </div>
                                                        <input type="date" class="form-control" id="validationCustomUsername" name="de" aria-describedby="inputGroupPrepend">

                                                    </div>
                                                </div>
                                                <div class="col mt-4 ">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="inputGroupPrepend">Ate</span>
                                                        </div>
                                                        <input type="date" class="form-control" id="validationCustomUsername" name="ate"  aria-describedby="inputGroupPrepend">

                                                        <button type="submit" class="btn-primary " aria-label="Alinhar na esquerda">
                                                            <span class="glyphicon glyphicon-search" >Filtrar</span>
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="card mt-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="peity_line_neutral left text-center mt-3">
                      <span
                      ><span style="display: none">10,15,20,25,21,35,40</span>
                        <canvas width="50" height="24"></canvas>
                      </span>

                                </div>
                            </div>
                            <div class="col-md-6 border-left text-center pt-2">
                                <h3 class="mb-0 fw-bold"><?php echo "R$ " . number_format($indicarores->filtrosIndicadores('ofertas'), 2, ',', '.'); ?></h3>
                                <span class="text-muted">Ofertas</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card mt-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="peity_bar_bad left text-center mt-3">
                      <span
                      ><span style="display: none">3,5,6,16,8,10,6</span>
                        <canvas width="50" height="24"></canvas>
                      </span>

                                </div>
                            </div>
                            <div class="col-md-6 border-left text-center pt-2">
                                <h3 class="mb-0 fw-bold"><?php echo "R$ " . number_format($indicarores->filtrosIndicadores('despesas'), 2, ',', '.'); ?></h3>
                                <span class="text-muted">Despesas</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card mt-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="peity_line_good left text-center mt-3">
                      <span
                      ><span style="display: none">12,6,9,23,14,10,17</span>
                        <canvas width="50" height="24"></canvas>
                      </span>

                            </div>
                            </div>
                            <div class="col-md-6 border-left text-center pt-2">
                                <h3 class="mb-0 fw-bold"><?php echo "R$ " . number_format($indicarores->filtrosIndicadores('dizimimos'), 2, ',', '.'); ?></h3>
                                <span class="text-muted">Dizimos</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card mt-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="peity_bar_good left text-center mt-3">
                                    <span>12,6,9,23,14,10,13</span>

                                </div>
                            </div>
                            <div class="col-md-6 border-left text-center pt-2">
                                <h3 class="mb-0 fw-bold"><?php echo "R$ " . number_format($indicarores->caixaComFiltro (), 2, ',', '.'); ?></h3>
                                <span class="text-muted">Caixa</span>
                            </div>
                        </div>
                    </div>

                </div>

                <hr>


                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Membos/Congregação/Ceadep </h4>
                        <div class="ms-auto text-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">

                                    <li class="breadcrumb-item active" aria-current="page">
                                        <form method="post" action="dashbord.php">
                                            <div class="row">

                                                <div class="col mt-4">


                                                </div>

                                            </div>
                                        </form>
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card mt-0">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="peity_bar_good left text-center mt-3">
                                        <span>1,2,3,4,5,6,7</span>
                                    </div>
                                </div>
                                <div class="col-md-6 border-left text-center pt-2">
                                  <h3 class="mb-0 fw-bold"><?php echo ""  . number_format($indicarores->quantidadesEmUmaDeterMinadaTabela('membros'), 2, '.' ,'' ); ?></h3>
                                    <span class="text-muted">Dizimista</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card mt-0">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="peity_line_good left text-center mt-3">
                                    <span><span style="display: none">1,2,3,4,5,6,7,8,100</span>
                                        <canvas width="50" height="24"></canvas>
                                    </span>

                                    </div>
                                </div>
                                <div class="col-md-6 border-left text-center pt-2">
                                <h3 class="mb-0 fw-bold"><?php echo ""  . number_format($indicarores->quantidadesEmUmaDeterMinadaTabela('igrejas'), 2, '.' ,'' ); ?></h3>
                                    <span class="text-muted">Congregações</span>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                    <div class="card mt-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="peity_line_good left text-center mt-3">
                      <span
                      ><span style="display: none">1,20,5,20,5,20,20,20,20,20</span>
                        <canvas width="50" height="24"></canvas>
                      </span>

                            </div>
                            </div>
                            <div class="col-md-6 border-left text-center pt-2">
                                <h3 class="mb-0 fw-bold"><?php echo "R$ " . number_format($indicarores->CalculoCeadepeValorCaixa(), 2, ',', '.'); ?></h3>
                                <span class="text-muted">10% do Caixa</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card mt-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="peity_bar_good left text-center mt-3">
                                    <span>13,1,1,1,1,1,0    </span>

                                </div>
                            </div>
                            <div class="col-md-6 border-left text-center pt-2">
                                <h3 class="mb-0 fw-bold"><?php echo "R$ " . number_format($indicarores->CalculoCeadepeValorComTodasAsReceitas (), 2, ',', '.'); ?></h3>
                                <span class="text-muted">10% da Receita</span>
                            </div>
                        </div>
                    </div>

                </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->



                </div>




<!--            -->
            <!-- End cards -->
            <!-- Chart-3 -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Bar Chart</h5>
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-line-chart"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--             End chart-3 -->
<!--             Charts -->
<!--            <div class="row">-->
<!--                <div class="col-md-6">-->
<!--                    <div class="card">-->
<!--                        <div class="card-body">-->
<!--                            <h5 class="card-title">Pie Chart</h5>-->
<!--                            <div class="pie" style="height: 400px"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-6">-->
<!--                    <div class="card">-->
<!--                        <div class="card-body">-->
<!--                            <h5 class="card-title">Line Chart</h5>-->
<!--                            <div class="bars" style="height: 400px"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <!-- End Charts -->
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->
            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
        </div>



        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->

        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </d
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
        <!-- ============================================================== -->
        <footer class="footer text-center">
            Isael Silva
            <a href="https://www.wrappixel.com">WrapPixel</a>.
        </footer>
<!-- ============================================================== -->
<script src="../front-end/Views/assets/libs/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="../front-end/Views/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="../front-end/Views/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
<script src="../front-end/Views/assets/extra-libs/sparkline/sparkline.js"></script>
<!--Wave Effects -->
<script src="../front-end/Views/dist/js/waves.js"></script>
<!--Menu sidebar -->
<script src="../front-end/Views/dist/js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="../front-end/Views/dist/js/custom.min.js"></script>
<!-- this page js -->
<script src="../front-end/Views/assets/libs/chart/matrix.interface.js"></script>
<script src="../front-end/Views/assets/libs/chart/excanvas.min.js"></script>
<script src="../front-end/Views/assets/libs/flot/jquery.flot.js"></script>
<script src="../front-end/Views/assets/libs/flot/jquery.flot.pie.js"></script>
<script src="../front-end/Views/assets/libs/flot/jquery.flot.time.js"></script>
<script src="../front-end/Views/assets/libs/flot/jquery.flot.stack.js"></script>
<script src="../front-end/Views/assets/libs/flot/jquery.flot.crosshair.js"></script>
<script src="../front-end/Views/assets/libs/chart/jquery.peity.min.js"></script>
<script src="../front-end/Views/assets/libs/chart/matrix.charts.js"></script>
<script src="../front-end/Views/assets/libs/chart/jquery.flot.pie.min.js"></script>
<script src="../front-end/Views/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="../front-end/Views/assets/libs/chart/turning-series.js"></script>
<script src="../front-end/Views/dist/js/pages/chart/chart-page-init.js"></script>
</body>
</html>
