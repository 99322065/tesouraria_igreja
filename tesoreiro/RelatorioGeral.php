<?php include '../Conexao/Conexao.php';

class RelatotioGeral extends Conexao
{

    private $id;

    private $de;

    private $ate;

    private $valor;

   /**
     * Get the value of de
     */ 
    public function getDe()
    {
        return $this->de;
    }

    /**
     * Set the value of de
     *
     * @return  self
     */ 
    public function setDe($de)
    {
        $this->de = $de;

        return $this;
    }

    /**
     * Get the value of ate
     */ 
    public function getAte()
    {
        return $this->ate;
    }

    /**
     * Set the value of ate
     *
     * @return  self
     */ 
    public function setAte($ate)
    {
        $this->ate = $ate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return RelatotioGeral
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


     /**
     * Get the value of valor
     */ 
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set the value of valor
     *
     * @return  self
     */ 
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }


    public function relatoriodedizimistas()
    {

        if($this->getDe() || $this->getAte){
            $de = $this->getDe();
            $ate = $this->getAte();

             $relatorioGeralDeDizimistas =
            "SELECT 
            dizimimos.id       AS id,
            nome_membro        AS Nome,
            nome_igreja        AS Igreja,
            mes                AS Mês ,
            data               AS Data,
            valor              AS Valor, 
            tipo.tipo          AS tipo
         FROM membros 
         inner join  dizimimos  on membros.id = dizimimos.fk_dizimista
         inner join igrejas on membros.fk_igreja = igrejas.id 
         inner join tipo on tipo.id = membros.cargo where data >= '$de' and data <= '$ate'
        ";
        $relatorioGeralDeDizimistas = $this->db->prepare($relatorioGeralDeDizimistas);
        $relatorioGeralDeDizimistas->execute();

        $relatorio = $relatorioGeralDeDizimistas->fetchAll();

    

        }else{
        $relatorioGeralDeDizimistas =
            "SELECT 
            dizimimos.id       AS id,
            nome_membro        AS Nome,
            nome_igreja        AS Igreja,
            mes                AS Mês ,
            data               AS Data,
            valor              AS Valor, 
            tipo.tipo          AS tipo
         FROM membros 
         inner join  dizimimos  on membros.id = dizimimos.fk_dizimista
         inner join igrejas on membros.fk_igreja = igrejas.id 
         inner join tipo on tipo.id = membros.cargo 
        ";
        $relatorioGeralDeDizimistas = $this->db->prepare($relatorioGeralDeDizimistas);
        $relatorioGeralDeDizimistas->execute();

         $relatorio = $relatorioGeralDeDizimistas->fetchAll();
        }

        return  $relatorio ;
    }


    public function relatorioDizimosMesAtual()
    {
        $mesAtual = date('Y-m');
        $relatorioGeralDeDizimistas =
            "SELECT 
            dizimimos.id       AS id,
            nome_membro        AS Nome,
            nome_igreja        AS Igreja,
            mes                AS Mês ,
            data               AS Data,
            valor              AS Valor,
            tipo.tipo          AS tipo
         FROM membros 
         left join  dizimimos  on membros.id = dizimimos.fk_dizimista
         left join igrejas on membros.fk_igreja = igrejas.id
         left join tipo on tipo.id = membros.cargo 
         where mes = :mes
        ";
        $relatorioGeralDeDizimistas = $this->db->prepare($relatorioGeralDeDizimistas);
        $relatorioGeralDeDizimistas->bindValue(':mes', $mesAtual);
        $relatorioGeralDeDizimistas->execute();

        return $relatorioGeralDeDizimistas->fetchAll();
    }

   
    public function valorTotalDizimosMesAtual()
    {
        $mesAtual = date('Y-m');
        $pegarValorTotalDizimos =
            "SELECT sum(valor)  from dizimimos where mes = :mes";
        $pegarValorTotalDizimos = $this->db->prepare($pegarValorTotalDizimos);
        $pegarValorTotalDizimos->bindValue(':mes', $mesAtual);
        $pegarValorTotalDizimos->execute();
        $pegarValorTotalDizimos = $pegarValorTotalDizimos->fetch()[0];

        $converterValorDecimal = number_format($pegarValorTotalDizimos, 2, ',', '.');
        return $converterValorDecimal;
    }


    public function quantidadeDeDizimistasNoMesAtual()
    {
        $mesAtual = date('Y-m');
        $pegarValorTotalDizimos =
            "SELECT count(id)  from dizimimos where mes = :mes";
        $pegarValorTotalDizimos = $this->db->prepare($pegarValorTotalDizimos);
        $pegarValorTotalDizimos->bindValue(':mes', $mesAtual);
        $pegarValorTotalDizimos->execute();
        $pegarValorTotalDizimos = $pegarValorTotalDizimos->fetch()[0];
        $converterValorDecimal = number_format($pegarValorTotalDizimos, 2, ',', '.');
        return $converterValorDecimal;
    }


    public function relatoriodedespesa()
    {
        $relatorioDedespesaDoMesComArquivos =
            "SELECT  despesas.id as id_despesa,nome_igreja,despesas.mes, despesas.data,despesas.valor,tipoDespesa,observacoes,arquivo
             FROM despesas INNER JOIN igrejas ON fk_igreja = igrejas.id";
        $relatorioDedespesaDoMesComArquivos = $this->db->prepare($relatorioDedespesaDoMesComArquivos);
        $relatorioDedespesaDoMesComArquivos->execute();
         $valor = $relatorioDedespesaDoMesComArquivos->fetchAll();

         return $valor;
        $this->pegarValorDespesaComFiltro($valor);
    }

    public function pegarValorDespesaComFiltro($valor){
        return $valor;
    }


    public function excluir($tabela){
        $id = $this->getId();
        $excluir = "DELETE FROM $tabela WHERE id=:id";
        $excluir = $this->db->prepare($excluir);
        $excluir->bindValue(':id',$id);
        $excluir->execute();
        if ($excluir->rowCount() > 0){
            header("location: relatorio_dizimo_mes.php?sucesso=ok");
        }else{
            header("location: relatorio_dizimo_mes.php?naoCadastrado=ok");
        }
    }


    public function excluirDizimos($tabela){
        $id = $this->getId();
        $excluir = "DELETE FROM $tabela WHERE id=:id";
        $excluir = $this->db->prepare($excluir);
        $excluir->bindValue(':id',$id);
        $excluir->execute();
        if ($excluir->rowCount() > 0){
            header("location: relatorio.php?sucesso=ok");
        }else{
            header("location: relatorio.php?naoCadastrado=ok");
        }
    }

 

   
}
