<?php 

    include 'CadastroIgreja.php';


     class Query extends CadastroIgreja{

        private $tabela;

        private $nome;

        /**
         * Get the value of nome
         */ 
        public function getNome()
        {
                return $this->nome;
        }

        /**
         * Set the value of nome
         *
         * @return  self
         */ 
        public function setNome($nome)
        {
                $this->nome = $nome;

                return $this;
        }
        

            /**
         * Get the value of tabela
         */ 
        public function getTabela()
        {
                return $this->tabela;
        }

        /**
         * Set the value of tabela
         *
         * @return  self
         */ 
        public function setTabela($tabela)
        {
                $this->tabela = $tabela;

                return $this;
        }


        public function selecionarIgrejaParaOcasastroDoDizimista(){
           $queryComAsIgrejas = $this->getSelect('igrejas');
           $arrayIgrjas = [];
           foreach ($queryComAsIgrejas as $igreja) {
               $queryComAsIgrejas = $igreja['nome_igreja'];
           }

           return $queryComAsIgrejas;
        }

        public function selecionarDizimista(){
            $queryComOsDizimistas = $this->getSelect('dizimistas');
            $queryComOsDizimistas = [];
            foreach ($queryComOsDizimistas as $dizimista) {
                $queryComdizimista = $dizimista['nome_igreja'];
            }

            return $queryComdizimista;
         }

         
         public function bucar()
         {
            $tabela = $this->getTabela();
            $nome   = $this->getNome();
            $quelry = "SELECT * FROM $tabela ";
            $quelry = $this->db->prepare($quelry);
            $quelry->execute();
                
            $queryComdizimista = [];
            $dados = $quelry->fetchAll();
            // foreach ($dados as $dizimista) {
            //     $queryComdizimista[] = $dizimista['nome_membro'];
            // }

         
            return $dados;

         }


    
    }

?>