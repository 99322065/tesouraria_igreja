<?php
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Snappy\Pdf;
class GeraPdf
{
    public function actionPdf($html)
    {
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);
        $dompdf->loadHtml("$html");
        $dompdf->setPaper('A4');
        $dompdf->render();
        $dompdf->stream('carta.pdf', array('Attachment' => false));
    }

    public function salvarPdf()
    {
        
    }

}