
<?php
require '../vendor/autoload.php';

//require __DIR__ . '/vendor/autoload.php';

use Knp\Snappy\Pdf;
include 'GeraPdf.php';



$tipoDaCarta         = $_POST['tipoCarta'];
$nomeIgrejaNova      = strtoupper ($_POST['igreja']);
$cargo               = strtoupper( $_POST['cargo']);
$membro              = strtoupper ($_POST['nome_membro']);
$dataBatismoNasAgua  = $_POST['batismo_agua'];


$formatter = new DateTime();
//$formatter = new IntlDateFormatter(
//    'pt_BR',
//    IntlDateFormatter::FULL,
//    IntlDateFormatter::NONE,
//    'America/Sao_Paulo',
//    IntlDateFormatter::GREGORIAN
//);
//var_dump('s');die();

$html = '
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carta</title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
 <body class="border">
<h1 class="container text-center">'.$tipoDaCarta.'</h1>
<div class="container text-center">
    <div class="">
        <img width="100%" src="https://i.ibb.co/j6VBC57/teste.png" alt="">
    </div>
    
</div>

     
    <div class="">
        <div>
            <img width="16%" class="rounded float-left" src="https://i.ibb.co/02Z4S3P/igrejas.png" alt"">
        </div
        <div>
            <img width="16%" class="rounded float-right" src="https://i.ibb.co/xGpkpjT/igrejas2.png">
        </div>
    </div>
    <div class="w-75">
    <div class="container">
        <h4 class="text-center">Saudações no Senhor</h4>
        <div class="container text-justify ">

            Prezados irmãos em Cristo Jesus
            Apresentamos a 
            <br>
            <strong>
            '.$nomeIgrejaNova.'
            </strong>
            <br>
            <br>
            O irmão(a) <strong>'.$membro.' </strong>,e <strong> '.$cargo.' </strong> desta igreja desde <strong>'.$dataBatismoNasAgua.'</strong>
            <br>
           <br>
            Por se achar em comunhão com esta igreja, nós o recomendamos para que
            <br>
            os recebais no Senhor como usam fazer os santos
            <br>
            <br>
            <br>
            <div class="text-center">
                Assembleia de Deus em Fronteiras-PI
            </div>
            <br>
            <div class="text-center">
                <strong>'.$formatter->format('d-m-Y').'</strong>

        </div>
        <div class="container text-center">
                <img width="40%" src="https://i.ibb.co/5kyFwHr/ass.png" alt="">
        </div>
    </div>
</div>
</body>
</html>
';

$gerarPdf = new GeraPdf();
$gerarPdf->actionPdf($html);
